from datasets_prepare import DatasetsPrepare

import numpy as np
from time import time

from sklearn import metrics
from sklearn.linear_model import RidgeClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestCentroid


#X_train, y_train, X_test, y_test = DatasetsPrepare().prepare()
#print len(X_train), len(y_train), len(X_test), len(y_test)

# data_example
'''
X_train = np.array([[1, 1], [2, 1], [10, 10], [20, 10]])
X_test = np.array([[1, 1], [2, 1], [10, 10], [20, 10]])
y_train = np.array([1, 1, 2, 2])
y_test = np.array([1, 1, 2, 2])

'''

def benchmark(clf):    
    t0 = time()
    clf.fit(X_train, y_train)
    train_time = time() - t0
    
    t0 = time()
    pred = clf.predict(X_test)
    test_time = time() - t0

    score = metrics.f1_score(y_test, pred)
    
    clf_descr = str(clf).split('(')[0]
    clf_name = clf_descr.split('(')[0]

    count = 0
    for x in range(len(y_test)):
    	if y_test[x] == pred[x]:
    		#print x, y_test[x], pred[x]
    		count+=1
    accuracy = float(count)*100/len(y_test)
    
    #print(clf)
    #print("train time: %0.3fs" % train_time)
    #print("test time:  %0.3fs" % test_time)
    #print("f1-score: %0.3f" % score)
    #print "accuracy: ", acc, "%"
    #print('_' * 80)# test doc classifier
    return clf_name, accuracy, clf_descr, score, train_time, test_time


global X_train, y_train, X_test, y_test


results = []
for i in range(7):
	X_train, y_train, X_test, y_test = DatasetsPrepare().prepare(i)
	
	clf = [
		RidgeClassifier(tol=1e-2, solver="lsqr"), 
		#Perceptron(n_iter=50), 
		#PassiveAggressiveClassifier(n_iter=50),
		#KNeighborsClassifier(n_neighbors=10), 
		#NearestCentroid(),
		#MultinomialNB(alpha=.01),
		#BernoulliNB(alpha=.01),
		#LinearSVC(loss='l2', penalty="l1",dual=False, tol=1e-3),
		#LinearSVC(loss='l2', penalty="l2",dual=False, tol=1e-3),
		#SGDClassifier(alpha=.0001, n_iter=50, penalty="l1"),
		#SGDClassifier(alpha=.0001, n_iter=50, penalty="l1"),
		#SGDClassifier(alpha=.0001, n_iter=50, penalty="elasticnet")
	]

	aresult = [benchmark(c) for c  in clf]
	results.append(aresult)

for i in range(7):
	print "emotion: ", i
	for c in range(len(results[i])):
		print results[i][c][0], results[i][c][1]

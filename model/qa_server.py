import collections
import math
import sys
import os
import random
import socket

from time import localtime, strftime, time

class Server:

	def __init__(self,port):

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		server_addr = ('localhost', port)
		self.sock.bind(server_addr)

		self.sock.listen(25)


	def run(self,function):
		try:

			while True:
				# get new connection
				connection, client_addr = self.sock.accept()
				try:
					data = connection.recv(5000)
					print 'get question [',data,'] from',client_addr
					ret = function(data)
					connection.send(ret)

				except Exception as e:
					print "Server error ",sys.exc_traceback.tb_lineno
					connection.send("Error " + str(e))
				finally:
					connection.close()

		except Exception as e:
			print "ERROR :",e


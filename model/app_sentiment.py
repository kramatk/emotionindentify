#-*-coding: utf-8 -*-
from datasets_prepare import DatasetsPrepare
from qa_server import Server
import json

import numpy as np
from time import time

from sklearn import metrics
from sklearn.linear_model import RidgeClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestCentroid


#X_train, y_train, X_test, y_test = DatasetsPrepare().prepare()
#print len(X_train), len(y_train), len(X_test), len(y_test)

# data_example
'''
X_train = np.array([[1, 1], [2, 1], [10, 10], [20, 10]])
X_test = np.array([[1, 1], [2, 1], [10, 10], [20, 10]])
y_train = np.array([1, 1, 2, 2])
y_test = np.array([1, 1, 2, 2])

'''

global X_train, y_train, X_test, y_test

def benchmark(clf):    
	t0 = time()
	clf.fit(X_train, y_train)
	train_time = time() - t0
	
	t0 = time()
	pred = clf.predict(X_test)
	test_time = time() - t0
	#print len(X_train),len(X_test)
	#score = metrics.f1_score(y_test, pred)
	
	clf_descr = str(clf).split('(')[0]
	clf_name = clf_descr.split('(')[0]

	count = 0
	for x in range(len(y_test)):
		if y_test[x] == pred[x]:
			#print x, y_test[x], pred[x]
			count+=1
	accuracy = float(count)*100/len(y_test)
	
	#print(clf)
	#print("train time: %0.3fs" % train_time)
	#print("test time:  %0.3fs" % test_time)
	#print("f1-score: %0.3f" % score)
	#print "accuracy: ", acc, "%"
	#print('_' * 80)# test doc classifier
	return clf_name, accuracy, clf_descr, train_time, test_time

def predict(model,data):   

	t0 = time()
	pred = model.predict(data)
	test_time = time() - t0

	return pred[0],test_time




print "== Set up model =="

def get_emo(emo):
	if(emo==0):
		return "ดีใจ";
	elif(emo==1):
		return "กลัว";
	elif(emo==2):
		return "โกรธ";
	elif(emo==3):
		return "อิจฉา";
	elif(emo==4):
		return "เสียใจ";
	elif(emo==5):
		return "ประหลาดใจ";
	elif(emo==6):
		return "ให้กำลังใจ";
	else: 
		return "ไม่รู้";

emo = [get_emo(i) for i in range(7)]
nEmotion = 7

datasets = []
models = []
accuracy = []

for i in range(nEmotion):
	datasets.append(DatasetsPrepare())
	X_train, y_train, X_test, y_test = datasets[i].prepare(i)

	clf = [
		RidgeClassifier(tol=1e-2, solver="lsqr"), 
		Perceptron(n_iter=50), 
		PassiveAggressiveClassifier(n_iter=50),
		KNeighborsClassifier(n_neighbors=10), 
		NearestCentroid(),
		MultinomialNB(alpha=.01),
		BernoulliNB(alpha=.01),
		#LinearSVC(loss='l2', penalty="l1",dual=False, tol=1e-3),
		LinearSVC(loss='l2', penalty="l2",dual=False, tol=1e-3),
		SGDClassifier(alpha=.0001, n_iter=50, penalty="l1"),
		SGDClassifier(alpha=.0001, n_iter=50, penalty="elasticnet")
	]
	aresult = [benchmark(c) for c  in clf]

	#choose model 
	m = 0
	for c in range(len(aresult)):
		if aresult[m][1] < aresult[c][1]:
			m = c

	print "\t",aresult[m][0], aresult[m][1]

	models.append(clf[m])
	accuracy.append(aresult[m][1])

	print " == build model "+str(int((i+1)*100/nEmotion)) +"% =="


def action(txt):
	s = []
	for i in range(nEmotion):
		inp = datasets[i].input_prepare(txt)
		ret,t = predict(models[i],inp)
		print emo[i],ret
		if ret == 1:
			s.append({"emo":emo[i],"acc":accuracy[i]})

	if len(s)==0:
		s.append("ไม่มีอารมณ์")

	return json.dumps(s, ensure_ascii=False)

port = 1888
print "== Estabish Server port",port
s = Server(port)
s.run(action)

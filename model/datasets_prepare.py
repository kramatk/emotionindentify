#-*-coding: utf-8 -*-
from kucut import SimpleKucutWrapper as KUCut
import os
import random

class DatasetsPrepare:	
	def __init__(self):
		self.emotion = 0
		self.attributes = {}
		self.emotional_word_enable = True
		
	def filepath(self, file_test):
		if os.path.isfile(os.path.abspath(file_test)):
			return os.path.abspath(file_test)

	def list_all_files_in_dir(self, dir_file_train):
		all_filenames = ["" for i in range(7)]

		for filename in os.listdir(dir_file_train):
			if os.path.isfile(os.path.join(dir_file_train, filename)):
				fname = os.path.abspath(os.path.join(dir_file_train, filename))
				all_filenames.append(fname)
				path = fname.split("/")
				filename = path[len(path)-1].split(".")[0]
				num = self.emotion_number(filename)
				all_filenames[num] = fname
				
		return all_filenames

	def emotion_number(self, emotion):
		no = None
		if emotion =='ดีใจ':
			no = 0
		elif emotion =='กลัว':
			no = 1
		elif emotion =='โกรธ':
			no = 2
		elif emotion =='อิจฉา':
			no = 3
		elif emotion =='เสียใจ':
			no = 4
		elif emotion =='ประหลาดใจ':
			no = 5
		elif emotion =='ให้กำลังใจ':
			no = 6
		return no

	def emotional_word(self, filepath):
		infile = open(filepath, 'r')
		for each_line in infile:
			word = each_line.split('\n')
			self.attributes[word[0]] = 0

	def train_prep(self, filepath):
		infile = open(filepath, 'r')
		element = []
		target = []
		for each_line in infile:
			snt = {}
			word = each_line.split(" ")
			for w in word:
				if not self.emotional_word_enable:
					self.attributes[w] = 0

				if w in snt:
					snt[w] += 1
				else:
					snt[w] = 1
			element.append(snt)
			target.append(self.emotion)	
		return (element, target)

	def test_prep(self, filepath, modelno):
		testfile = open(filepath,'r')
		element , target = [], []
		pos = []
		neg = []
		pos_target = []
		neg_target = []

		for each_line in testfile:
			seperator = each_line.split('#')
			
			#target.append(self.emotion_number(seperator[0]))
			no = self.emotion_number(seperator[0])
			
			word = seperator[1].split(' ')
			snt = {}
			for w in word:
				if w in snt:			
					snt[w] += 1
				else:
					snt[w] = 1

			if no != modelno:
				no = 0
				pos.append(snt)
				pos_target.append(no)
			else:
				no = 1
				neg.append(snt)
				neg_target.append(no)

			#target.append(no)
			#element.append(snt)
		if len(pos) < len(neg):
			element , target =  self.equality(pos,neg,pos_target,neg_target)
		else:
			element , target = self.equality(neg,pos,neg_target,pos_target)

		return (element, target)
		
	def data_prep(self, element):
		data = []
		for elem in element:
			text = []
			for key,val in self.attributes.items():
				if key in elem:
					text.append(float(elem[key]))
				else:
					text.append(0.0)
			data.append(text)
		return data

	def input_prepare(self,txt):

		#txt = txt.encode('utf-8')
		txt = txt.strip()
		myKUCut = KUCut()
		token_txt = []
	
		try:
			s = ""
			result = myKUCut.tokenize([txt])
			for w in  result[0][0]:
				w = w.encode('utf-8')
				if w!= "_":
					token_txt.append(w)
					s+=w+" "
		except Exception as e:
			token_txt = []
			print "Tokenizer error ",e
			
		element = []
		target = []
		snt = {}
		word = token_txt
		for w in word:
			if w in snt:
				snt[w] += 1
			else:
				snt[w] = 1
		element.append(snt)

		data = self.data_prep(element)
		return (data)

	def equality(self,less,more,less_tar,more_tar):
		instance = []
		target = []
		instance.extend(less)
		target.extend(less_tar)
		#print len(less),len(more)
		for index in range(len(less)):
			index_more = random.randint(0,len(more)-1)
		
			instance.append(more[index_more])
			target.append(more_tar[index_more])
		return instance,target


	def prepare(self, modelno):
		data_train = []
		data_test = []
		target_train = []
		target_test = []		
		instance = []

		filepath_test = self.filepath('../test.txt')
		allfiles = self.list_all_files_in_dir('../data-full')

		if self.emotional_word_enable:
			self.emotional_word('../emotion_word.txt')


		pos = []
		neg = []
		pos_target = []
		neg_target = []
		for index in range(len(allfiles)):
			if index == modelno:
				self.emotion = 1
			else:
				self.emotion = 0

			element , target = self.train_prep(allfiles[index])

			if self.emotion ==1:
				pos.extend(element)
				pos_target.extend(target)
			else:
				neg.extend(element)
				neg_target.extend(target)

			
			#self.emotion += 1

		if len(pos) < len(neg):
			instance , target_train =  self.equality(pos,neg,pos_target,neg_target)
		else:
			instance , target_train = self.equality(neg,pos,neg_target,pos_target)


		data = self.data_prep(instance)
		for each in data:
			data_train.append(each)

		element_test, target_test = self.test_prep(filepath_test, modelno)
		data_test = self.data_prep(element_test)

		return (data_train, target_train, data_test, target_test)

# how to use
#data_train, target_train, data_test, target_test = DatasetsPrepare().prepare(0)
#print len(data_train), len(target_train), len(data_test), len(target_test)

#class input
#filedir
#print sys.argv[1] = testfile

# class output
#X_train = data
#X_test = test_data
#y_train = target
#y_test = test_target

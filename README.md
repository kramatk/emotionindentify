# Emotion Identifying #

Application that can identify text emotion base on `THAI language`

# Introduction #
**reference** [Identifying Expressions of Emotion in Text ](http://www-scf.usc.edu/~saman/pubs/2007-TSD-paper.pdf)

We use corpus from comment in `Pantip.com` , that is most popular webboard in Thai

we used `Naïve Bayes`, and `Support Vector 
Machines` , which have been popularly used in sentiment classification tasks



# Tools #
[http://scikit-learn.org/](http://scikit-learn.org/stable/auto_examples/document_classification_20newsgroups.html#example-document-classification-20newsgroups-py)

# HOW TO RUN #
1. download [kucut](https://bitbucket.org/veer66/kucut/src/) ไว้ใน folder เดียวกับโปรเจคนี้
2. cd `model` directory
3. run `python app_sentiment.py ` รอจนเห็น 	`Estabish Server port 1888`
4. copy หรือ สร้าง shortcut สำหรับ folder `app` ไปไว้ใน XAMPP หรือ directory สำหรับ localhost
5. เปิด localhost ไปยัง URL ที่ได้จากในข้อ 4 [like this](https://bitbucket.org/repo/joAE5q/images/3979489284-Screenshot%20from%202014-12-13%2003:55:58.png)

# SUMMARY #
	RidgeClassifier 16.0
	Perceptron 6.0
	PassiveAggressiveClassifier 46.0
	KNeighborsClassifier 46.0
	NearestCentroid 46.0
	NaiveBayes MultinomialNB 16.0
	NaiveBayes BernoulliNB 16.0
	LinearSVC 16.0
	SGDClassifier 6.0
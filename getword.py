 # -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup as bs
from kucut import SimpleKucutWrapper as KUCut
import sys
import re
import json
import subprocess

debug = False

def req(start):
	print " START ",start
	r = requests.get("http://curve.in.th/emotion/sample/word/"+str(start))
	r.encoding = "utf-8"
	data = json.loads(r.text.encode("utf-8"))
	return data

def get():

	fo = open("emotion_word.txt","w")
	myKUCut = KUCut()

	data = req(0)
	nn = data["next"]
	prv = 0
	while nn > prv:
		prv = data["next"]

		for word in data["data"]:
			#print comment
			w = word.encode('utf-8')
			w = w.strip().replace("\n","").replace("&nbsp;"," ")
			fo.write(w+"\n")

		data = req(nn)
		nn = data["next"]
		
	fo.close();
	return True

get()

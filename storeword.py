 # -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup as bs
from kucut import SimpleKucutWrapper as KUCut
import sys
import re
import json
import subprocess
import time


debug = False

def get(emo):

	r = requests.get("http://curve.in.th/emotion/sample/0/"+str(emo))
	r.encoding = "utf-8"
	data = json.loads(r.text.encode("utf-8"))
		
	myKUCut = KUCut()
	print "get data"
	for comment in data["data"]:
		c = comment.encode('utf-8')
		c = c.strip().replace("\n","")
		try:
			result = myKUCut.tokenize([c])
			s = ""
			for w in  result[0][0]:
				w = w.encode('utf-8')
				if w!= "_":
					s+= w+" "
					store(w,emo)
		except Exception as e:
			print "error ",e,c

	return True

def store(word,i):
	payload = {'word': word }
	rr = requests.post("http://curve.in.th/emotion/store/word", data=payload)
	print "[WORD] ",rr.text,"   # ",i

	time.sleep(0.1)
	return rr.text

for i in range(6,0,-1):
	print " === === === emo",i," === === ==="
	get(i)

print " === === === emo","7"," === === ==="
get(7)

 # -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup as bs
from kucut import SimpleKucutWrapper as KUCut
import sys
import re
import json
import subprocess

debug = False

def get_emo(emo):
	if(emo==1):
		return "ดีใจ";
	elif(emo==2):
		return "เสียใจ";
	elif(emo==3):
		return "ให้กำลังใจ";
	elif(emo==4):
		return "อิจฉา";
	elif(emo==5):
		return "โกรธ";
	elif(emo==6):
		return "กลัว";
	elif(emo==7):
		return "ประหลาดใจ";
	elif(emo==8):
		return "ไม่มีอารมณ์";
	else: 
		return "ไม่รู้";


def req(start,emo):
	print "EMO",get_emo(emo)," START ",start
	r = requests.get("http://curve.in.th/emotion/sample/"+str(start)+"/"+str(emo))
	r.encoding = "utf-8"
	data = json.loads(r.text.encode("utf-8"))
	return data

def get(emo):

	fo = open("data/"+get_emo(i)+".txt","w")
	myKUCut = KUCut()

	data = req(0,emo)
	nn = data["next"]
	prv = 0
	while nn > prv:
		prv = data["next"]

		for comment in data["data"]:
			#print comment
			c = comment.encode('utf-8')
			c = c.strip().replace("\n","").replace("&nbsp;"," ")
			try:
				result = myKUCut.tokenize([c])
				s = ""
				for w in  result[0][0]:
					w = w.encode('utf-8')
					if w!= "_":
						s+= w+" "

				fo.write(s+"\n")
			except Exception as e:
				print "error ",e,c


		data = req(nn,emo)
		nn = data["next"]
		
	fo.close();
	return True

for i in range(1,8):
	print get_emo(i);
	get(i)

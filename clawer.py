import requests
import time
from bs4 import BeautifulSoup as bs
import sys
import re
import json
import subprocess

debug = False

def store(pantip_id,title):
	payload = {'pantip_id': pantip_id, 'title': title }
	#print pantip_id,title
	r = requests.post("http://curve.in.th/emotion/store/topic", data=payload)
	print "[TOPIC] ",r.text

	if "REPEAT" in r.text:
		if not debug:
			return False

	if not debug:
		subprocess.call(["phantomjs", "clawer.js","http://pantip.com/topic/"+str(pantip_id)])

	fo = open("output.txt")
	comments = fo.read().split("###")
	for c in comments:
		com = c.strip()
		if len(com) <= 1:
			continue
		
		payload = {'pantip_id': pantip_id, 'comment': com }
		rr = requests.post("http://curve.in.th/emotion/store/comment", data=payload)
		print "[COMMENT] ",rr.text
		time.sleep(5)

		if debug:
			break
	return True

def extract(HTML):
	soup = bs(HTML)
	post = soup.findAll("div", { "class" : "post-pick" })
	
	ret = []
	for p in post:
		title= p.findAll("div", { "class" : "post-pick-title" })[0].text
		pantip_id = p.findAll("a")[0]["href"].split("/")[2]	
		store(pantip_id,title)
		if debug:
			break

	return ret

def next_(HTML):
	soup = bs(HTML)
	nex = soup.findAll("div", { "class" : "loadmore-bar" })[0]
	return nex.findAll("a")[0]["href"].split("=")[1]

def more_(nex):
	url = "http://pantip.com/home/ajax_loadmore/?p="+nex
	r = requests.get(url)
	r.encoding = 'utf-8'
	js = json.loads(r.text)
	for topic in js["pick3"]:
		store(topic["_id"],topic["disp_topic"])
	print js["last_time"]
	return str(js["last_time"])
	

try:
	#r = requests.get('http://pantip.com')
	#r.encoding = 'utf-8'
	#extract(r.text)
	init = "1415939280-32000"  #next_(r.text)
	print "Init",init
	while(True):
		if debug:
			break
		init = more_(init)
		time.sleep(0.2)
		print " .... delay ...."
	
	
except Exception as e:
	print "line ",sys.exc_traceback.tb_lineno 
	print "Error",e
	         
print "[end]"

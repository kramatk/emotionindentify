<html>
<head>
	<title> What's your emotion??</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/jquery.min.js"></script>
	
</head>
<body>
	<div class="container-fluid">
    	<div class="row" style="text-align: center;">
      		<div class="col-md-6 col-md-offset-3">
        		<h1>Emotion</h1>
    			
					<form method="POST" id="nlp" accept-charset="UTF-8" class="form-inline" role="form">
						<div class="form-group">
  							<input class="form-control" id="txt" placeholder="Enter your text" style="width: 400px;height: initial;margin: auto;" name="txt" type="text" required>
  						</div>
				    	<input class="btn btn-default" type="submit" value="GET">

					</form>
					<ul class="list-group" id="emotion">
					    
					</ul>
        	</div>



        	
		</div>
	</div>
	<script>
		$(function(){

			$('#nlp').submit(function(event){
			    event.preventDefault();
			    var txt = $("#txt").val();
				console.log("send : "+txt);
			    $.post("client.php",{txt:txt},function(e){
					
				    try {
				        emo = JSON.parse(e);
				        
				        $("#emotion").fadeOut(500,function(){
				        	$("#emotion").html("");
				        	for(i=0;i<emo.length;i++){
					        	$("#emotion").append('<li class="list-group-item">'+emo[i]["emo"]+" "+emo[i]["acc"].toFixed(2)+'%</li>')
					        }
					        $("#emotion").fadeIn();
				        });
				        console.log(emo)
				    } catch (e) {
				        alert("Error "+e)
				    }
					
				})
			});
		})
	</script>
</body>
</html>
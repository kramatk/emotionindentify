 # -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup as bs
from kucut import SimpleKucutWrapper as KUCut
import sys
import re
import json
import subprocess

debug = False

def get_emo(emo):
	if(emo==1):
		return "ดีใจ";
	elif(emo==2):
		return "เสียใจ";
	elif(emo==3):
		return "ให้กำลังใจ";
	elif(emo==4):
		return "อิจฉา";
	elif(emo==5):
		return "โกรธ";
	elif(emo==6):
		return "กลัว";
	elif(emo==7):
		return "ประหลาดใจ";
	elif(emo==8):
		return "ไม่มีอารมณ์";
	else: 
		return "ไม่รู้";


def get():

	r = requests.get("http://curve.in.th/emotion/test/json")
	r.encoding = "utf-8"
	data = json.loads(r.text.encode("utf-8"))
	
	fo = open("test.txt","w")
	myKUCut = KUCut()


	for d in data:
		#print comment
		comment = d["comment"]
		emo = d["emo"].encode('utf-8')
		c = comment.encode('utf-8')
		c = c.strip().replace("\n","")
		try:
			result = myKUCut.tokenize([c])
			s = emo+"#"
			for w in  result[0][0]:
				w = w.encode('utf-8')
				if "#" in w:
					continue
				if w!= "_" :
					s+= w+" "
			#print s
			fo.write(s+"\n")
		except Exception as e:
			print "error ",e,c

	fo.close();
	return True

get()
